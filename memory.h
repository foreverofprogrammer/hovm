//
// Created by lee on 2024年1月24日 0024.
//

#ifndef HOVM_MEMORY_H
#define HOVM_MEMORY_H

typedef struct Memory {
    unsigned char *memory;
    unsigned long size;
} Memory;

Memory create_memory(unsigned long memory_size);
void release_memory(Memory memory);
unsigned char get_memory(Memory memory, unsigned long address);

#endif //HOVM_MEMORY_H
