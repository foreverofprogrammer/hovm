//
// Created by lee on 2024年1月29日 0029.
//

#ifndef HOVM_VMCPP_H
#define HOVM_VMCPP_H

#define TRUE (-1)
#define FALSE (-2)

typedef struct Add {
    unsigned int S;
    unsigned char C;
} Add;

typedef struct Div {
    unsigned short Q;  // 商
    unsigned short R;  // 余数
} Div;


typedef struct registers {
    unsigned char R[8];
    unsigned char BP, SP, SI, DI, CS, DS, SS, ES, IP;
    bool ZF, CF, SF, OF, TF;
} registers;


typedef struct Memory {
    unsigned char *memory;
    unsigned long size;
} Memory;


enum CMDID {
    NONE,
    MOV,
    ADD,
    SUB,
    MUL,
    DIV,
    SHL,
    SHR,
    INT,
    JMP,
    JZ,
    JNZ,
    CMP,
    INC,
    DEC,
    AND,
    OR,
    NOT,
    XOR,
    PUSH,
    POP,
};

typedef struct SourceRet {
    bool ret_true;
    unsigned char source;
} SourceRet;

typedef struct Command {
    unsigned char length;
    enum CMDID cmd;
    char offset;
    unsigned char target_type;
    unsigned char source_type;
    unsigned short target;
    unsigned short source;
    unsigned char INT;
} Command;

typedef struct CommandRet {
    Command *cmd;
    unsigned long long int size;
} CommandRet;

typedef Add (*add_func)(unsigned short add1, unsigned short add2);
typedef unsigned short (*sub_func)(unsigned short sub1, unsigned short sub2);
typedef unsigned short (*mul_func)(unsigned short mul1, unsigned short mul2);
typedef Div (*division_func)(unsigned short div1, unsigned short div2);
typedef void (*init_registers_func)(registers *r);
typedef void (*show_registers_func)(registers r);
typedef void (*update_register_func)(registers *r, unsigned char r_id, unsigned char r_value);
typedef unsigned char (*get_register_func)(registers r, unsigned char r_id, unsigned char default_value);
typedef CommandRet (*parse_command_func)(const unsigned char *command, long command_size);
typedef int (*execute_command_func)(Command cmd, registers *r, Memory memory);
typedef long int (*filesize_func)(const char *filename);
typedef unsigned char* (*file_get_binary_func)(const char *filename);
typedef void (*vmfree_func)(void *memory);
typedef Memory (*create_memory_func)(unsigned long memory_size);
typedef void (*release_memory_func)(Memory memory);
typedef unsigned char (*get_memory_func)(Memory memory, unsigned long address);

#endif //HOVM_VMCPP_H
