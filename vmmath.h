//
// Created by lee on 2024年1月24日 0024.
//

#ifndef HOVM_VMMATH_H
#define HOVM_VMMATH_H

typedef struct Add {
    unsigned int S;
    unsigned char C;
} Add;

typedef struct Div {
    unsigned short Q;  // 商
    unsigned short R;  // 余数
} Div;

Add add(unsigned short add1, unsigned short add2);
unsigned short sub(unsigned short sub1, unsigned short sub2);
unsigned short mul(unsigned short mul1, unsigned short mul2);
Div division(unsigned short div1, unsigned short div2);

#endif //HOVM_VMMATH_H
