//
// Created by lee on 2024年1月22日 0022.
//

#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include "file.h"

long int filesize(const char *filename) {
    FILE *file = fopen(filename, "rb");
    fseek(file, 0, SEEK_END);
    long int size = ftell(file);
    fclose(file);
    return size;
}

unsigned char* file_get_binary(const char *filename) {
    FILE *file = fopen(filename, "rb");
    long int size = filesize(filename);
    unsigned char *buffer = (unsigned char*) malloc(sizeof(unsigned char) * (size + 1));
    memset(buffer, 0, size + 1);
    fread(buffer, size, 1, file);
    buffer[size] = '\0';
    fclose(file);
    return buffer;
}
