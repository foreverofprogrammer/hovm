//
// Created by lee on 2024年1月22日 0022.
//

#include <stdio.h>
#include "registers.h"

void init_registers(registers *r) {
    for (int i = 0; i < 8; ++i) {
        (*r).R[i] = 0;
    }
    (*r).ZF = false;
    (*r).CF = false;
    (*r).SF = false;
    (*r).OF = false;
    (*r).TF = false;

    (*r).BP = 0;
    (*r).SP = 0;
    (*r).SI = 0;
    (*r).DI = 0;
    (*r).CS = 0;
    (*r).DS = 0;
    (*r).SS = 0;
    (*r).ES = 0;
    (*r).IP = 0;
}

void show_registers(registers r) {
    for (int i = 0; i < 8; ++i) {
        printf("R%d: %u  ", i, r.R[i]);
    }
    printf("\nZF: %u  TF: %u  SF: %u  OF: %u  CF: %u", r.ZF, r.TF, r.SF, r.OF, r.CF);
    printf("\nBP: %u  SP: %u  SI: %u  DI: %u  CS: %u  DS: %u  SS: %u  ES: %u  IP: %u\n",
           r.BP, r.SP, r.SI, r.DI, r.CS, r.DS, r.SS, r.ES, r.IP);
}

void update_register(registers *r, unsigned char r_id, unsigned char r_value) {
    if (r_id > 7) {
        return;
    }
    (*r).R[r_id] = r_value;
}

unsigned char get_register(registers r, unsigned char r_id, unsigned char default_value) {
    if (r_id > 7) {
        return default_value;
    }
    return r.R[r_id];
}
