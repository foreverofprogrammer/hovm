//
// Created by lee on 2024年1月24日 0024.
//

#include <malloc.h>
#include "memory.h"

Memory create_memory(unsigned long memory_size) {
    Memory memory;
    memory.memory = (unsigned char*)malloc(sizeof(unsigned char) * memory_size);
    memory.size = memory_size;
    return memory;
}

void release_memory(Memory memory) {
    free(memory.memory);
}

unsigned char get_memory(Memory memory, unsigned long address) {
    return memory.memory[address];
}
