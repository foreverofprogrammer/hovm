//
// Created by lee on 2024年1月22日 0022.
//

#ifndef HOVM_REGISTERS_H
#define HOVM_REGISTERS_H

#include <stdbool.h>

typedef struct registers {
    unsigned char R[8];
    unsigned char BP, SP, SI, DI, CS, DS, SS, ES, IP;
    bool ZF, CF, SF, OF, TF;
} registers;

void init_registers(registers *r);
void show_registers(registers r);
void update_register(registers *r, unsigned char r_id, unsigned char r_value);
unsigned char get_register(registers r, unsigned char r_id, unsigned char default_value);

#endif //HOVM_REGISTERS_H

/**
 * 状态标志位
 * CF(Carry Flag)（进位标志）
 *  当设置CF=1时，算术操作最高位产生了进位或借位；
 *  当设置CF=0时，算术操作最高位无进位或借位 ；
 * PF(Parity Flag)（奇偶标志）
 *  当设置PF=1时，数据最低8位中1的个数为偶数；
 *  当设置PF=0时，数据最低8位中1的个数为奇数；
 * AF(Auxiliary Flag)（辅助进位标志）
 *  当设置AF=1时，D3→D4位产生了进位或借位；
 *  当设置AF=0时，D3→D4位无进位或借位；
 * ZF(Zero Flag)（零标志）
 *  当设置ZF=1时，操作结果为0；
 *  当设置ZF=0时，结果不为0；
 * SF(Sign Flag)（符号标志）
 *  当设置SF=1时，结果最高位为1；
 *  当设置SF=0时，结果最高位为0；
 * OF(Overflow Flag)（溢出标志）
 *  当设置OF=1时，此次运算发生了溢出；
 *  当设置OF=0时，无溢出；
 * 控制标志位
 * DF(Direction Flag)（方向标志）
 *  用来控制数据串操作指令的步进方向；
 *  当设置DF=1时，将以递减顺序对数据串中的数据进行处理。
 *  当设置DF=0时，递增。
 * IF(Interrupt Flag)（中断允许标志）
 *  当设置IF=1时，开中断，CPU可响应可屏蔽中断请求；
 *  当设置IF=0时，关中断，CPU不响应可屏蔽中断请求。
 * TF(Trap Flag)（陷阱标志）
 *  为程序调试而设的。
 *  当设置TF=1时，CPU处于单步执行指令的方式；
 *  当设置TF=0时，CPU正常执行程序。
 */
