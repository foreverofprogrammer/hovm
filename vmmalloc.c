//
// Created by lee on 2024年1月24日 0024.
//

#include "vmmalloc.h"

void vmfree(void *memory) {
    free(memory);
}
