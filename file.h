//
// Created by lee on 2024年1月22日 0022.
//

#ifndef HOVM_FILE_H
#define HOVM_FILE_H

long int filesize(const char *filename);
unsigned char* file_get_binary(const char *filename);

#endif //HOVM_FILE_H
