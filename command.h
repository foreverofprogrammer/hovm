//
// Created by lee on 2024年1月23日 0023.
//

#ifndef HOVM_COMMAND_H
#define HOVM_COMMAND_H

#include <stdbool.h>
#include "registers.h"
#include "memory.h"
#include "vmmath.h"

#define TRUE (-1)
#define FALSE (-2)

enum CMDID {
    NONE,
    MOV,
    ADD,
    SUB,
    MUL,
    DIV,
    SHL,
    SHR,
    INT,
    JMP,
    JZ,
    JNZ,
    CMP,
    INC,
    DEC,
    AND,
    OR,
    NOT,
    XOR,
    PUSH,
    POP,
};

typedef struct SourceRet {
    bool ret_true;
    unsigned char source;
} SourceRet;

typedef struct Command {
    unsigned char length;
    enum CMDID cmd;
    char offset;
    unsigned char target_type;
    unsigned char source_type;
    unsigned short target;
    unsigned short source;
} Command;

typedef struct CommandRet {
    Command *cmd;
    unsigned long long int size;
} CommandRet;

CommandRet parse_command(const unsigned char *command, long command_size);
int execute_command(Command cmd, registers *r, Memory memory);

#endif //HOVM_COMMAND_H

/**
 * _,_,_,_
 * 1 command length
 * 2 command id
 * 3 target operator  00ismemory_isregister value
 * 4 source  00ismemory_isregister value
 */

/**
 * command id
 * 1 mov
 * 2 add
 * 3 sub
 * 4 mul
 * 5 div
 * 6 shl
 * 7 shr
 * 8 int
 * 9 jmp
 * 10 jz
 * 11 jnz
 * 12 cmp
 * 13 inc
 * 14 dec
 * 15 and
 * 16 or
 * 17 not
 * 18 xor
 * 19 push
 * 20 pop
 */
