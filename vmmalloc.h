//
// Created by lee on 2024年1月24日 0024.
//

#ifndef HOVM_VMMALLOC_H
#define HOVM_VMMALLOC_H

#include <malloc.h>

void vmfree(void *memory);

#endif //HOVM_VMMALLOC_H
